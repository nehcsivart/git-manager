# Git Manager

Python script to generate scripts for managing multiple Git repositories.

Basic Usage:

    python gen.py <config_file>

Format of configuration file is:

    directory_1: repository_1
    directory_2:
      - repository_2.1
      - repository_2.2
    directory_3:
      directory_3.1: repository_3.1
      directory_3.2:
        - repository_3.2.1
        - repository_3.2.2

This script will generate:

  * `create.sh`
    * creates repositories and directories if they do not exist
    * deletes git repositores if they exist
    * clones repository to their respective directories
    * self-deleting
  * `update.sh`
    * performs `git pull` on each repository
  * `ignore.sh`
    * adds git repositories to root .gitignore
    * self-deleting

Custom Usage:

    python gen.py --help

Please read the command line parameters via the `--help` flag.
Custom options include:

  * Automatically executing `create.sh` and `ignore.sh`.
  * Recursively check clonsed directories for `.gitrepo` configuration files.
    * note: not implemented yet


## Notes

  * The `create.sh` will only delete/modify specified git repositories.
  * The `ignore.sh` will not add duplicates nor delete anything.
  * Each non-specified directory can include other artifacts.
  * YAML is a superset of JSON, so your configuration file can be JSON as well.
  * PyYAML is used by default. JSON module is used if PyYAML is unavailable.


## Use Case
Suppose you are taking a class that has multiple assignments. For each
assignment, you prefer to use a Latex document on Overleaf. However, you also
want to have a main repository for the class as a whole.

Suppose you are taking a class with multiple group projects. For each group
project, the manager of the project repository is different, on different
server, and under different accounts.

The `git-manager` helps you in both cases by allowing you to create the main
repository, have a single configuration file point to each of the git
repositories, and a couple commands to clone and update all of them at once.







