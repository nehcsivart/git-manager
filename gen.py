
import argparse
import os
import subprocess
import stat
import sys



arg_parser = argparse.ArgumentParser()
arg_parser.add_argument( "config_file",
                         help="configuration file (see documentation for details)" )
arg_parser.add_argument( "-a",
                         "--autorun",
                         help="automatically executes \"create.sh\" and \"ignore.sh\"",
                         action="store_true" )
arg_parser.add_argument( "-R",
                         "--recursive",
                         help="recursively executes all cloned repos",
                         action="store_true" )
args = arg_parser.parse_args()




try:
  import yaml
  loader = yaml
  LoadError = yaml.YAMLError
except ImportError:
  sys.stderr.write("Warning: Using <json> As Loader\n")
  import json
  loader = json
  LoadError = ValueError



with open(args.config_file) as stream:
  try:
    config = loader.load(stream)
  except LoadError as e:
    sys.stderr.write("Error Loading Configuration\n")
    sys.stderr.write("{}\n".format(e))
    exit(1)


def recursive_write(config, stream_list, dir_path='.'):
  c_fmt = "git clone {} {} 2>/dev/null 1>&2 &\n"
  i_fmt = "{}\n"
  u_fmt = "git -C {} pull 2>/dev/null 1>&2 &\n"
  create = stream_list[0]
  ignore = stream_list[1]
  update = stream_list[2]
  for (key, value) in config.items():
    path = '/'.join([dir_path, key])
    create.write("# {}\n".format(key))
    if type(value) == str:
      create.write("rm -rf {}\n".format(path))
      create.write(c_fmt.format(value, path))
      ignore.write(i_fmt.format(path[2:]))
      update.write(u_fmt.format(path))
    elif type(value) == list:
      create.write("mkdir -p {}\n".format(path))
      num = 0
      for link in value:
        num += 1
        num_path = '/'.join([path, str(num)])
        create.write("# {}/{}\n".format(key, num))
        create.write("rm -rf {}\n".format(num_path))
        create.write(c_fmt.format(link, num_path))
        ignore.write(i_fmt.format(num_path[2:]))
        update.write(u_fmt.format(num_path))
    elif type(value) == dict:
      create.write("mkdir -p {}\n".format(path))
      recursive_write(value, stream_list, path)
    else:
      sys.stderr.write("invalid configuration\n")
      sys.stderr.write(value)
      sys.stderr.write("\n")
      exit(1)


def initialize(config):
  with open("create.sh", "w") as create, \
       open("ignore.sh", "w") as ignore, \
       open("update.sh", "w") as update:

    stream_list = [ create, ignore, update ]

    # header code for stream_list
    for stream in stream_list:
      stream.write("#/bin/sh\n")

    # header code for ignore.sh
    ignore.write( "temp=$(mktemp)\n"
                  "\n"
                  "cat <<EOF >$temp\n"
                  "update.sh\n" )

    # write out body code
    recursive_write(config, stream_list)

    # footer code for ignore.sh
    ignore.write( "EOF\n"
                  "\n"
                  "cat $temp .gitignore 2>/dev/null | "
                    "sort | uniq >.gitignore\n"
                  "\n"
                  "rm $temp\n"
                  "rm ignore.sh\n" )

    # footer code for create.sh
    create.write("rm create.sh\n")

  # allow execution
  for script in ["create.sh", "ignore.sh", "update.sh"]:
    os.chmod(script, stat.S_IRUSR | stat.S_IWUSR | stat.S_IXUSR)


initialize(config)

if args.autorun:
  subprocess.call(["sh", "create.sh"])
  subprocess.call(["sh", "ignore.sh"])







